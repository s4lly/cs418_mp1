# CS418: MP1 #

### Set up: ###
No compilation is necessary, simply open the ".html" file with a WebGL
compliant browser.

See the [link](http://www.khronos.org/webgl/wiki/Getting_a_WebGL_Implementation)
for information on currently supported browsers.

**note**
limiting the frame rate is not natively available when using
requestAnimationFrame(), therefore using workaround as outlined at
[creativejs](http://creativejs.com/resources/requestanimationframe/).
Otherwise, runs smoothly at 60fps.

### Video ###
[video link](http://youtu.be/KWwtx0db7a8)
