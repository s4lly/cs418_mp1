var VSHADER_SOURCE =
  'attribute vec4 a_VertexPosition;\n' +
  'attribute vec4 a_Color;\n' +
  'uniform mat4 u_MVMatrix;\n' +
  'uniform mat4 u_PMatrix;\n' +
  'varying vec4 v_Color;\n' +
  'void main() {\n' +
  '  gl_Position = u_PMatrix * u_MVMatrix * a_VertexPosition;\n' +
  '  v_Color = a_Color;\n' +
  '}\n';

var FSHADER_SOURCE =
  'precision mediump float;\n' +
  'varying vec4 v_Color;\n' +
  'void main() {\n' +
  '  gl_FragColor = v_Color;\n' +
  '}\n';

function main() {
  // Get canvas object
  var canvas = document.getElementById("webgl");

  // Get WebGL context
  var gl = getWebGLContext(canvas);
  if (!gl) {
	console.log("Failed to get the rendering context for WebGL");
	return;
  }

  // Initialize shaders
  if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
	console.log("Failed to initialize shaders");
	return;
  }

  gl.clearColor(0.0, 0.0, 0.0, 1.0);  // Clear to black
  gl.enable(gl.DEPTH_TEST);           // Enable depth testing

  // Setup vertices
  initBuffers(gl);

  // get uniform variable for model view and projection matrix 
  // and set current angle of rotation
  var u_MVMatrix = gl.getUniformLocation(gl.program, "u_MVMatrix");
  var u_PMatrix = gl.getUniformLocation(gl.program, "u_PMatrix");
  var angle = 0.0;

  var fps = 56;

  var render = function() {
	requestAnimationFrame(render);
	angle = animate(angle);
	drawScene(gl, angle, u_MVMatrix, u_PMatrix);
  };
  render();
}

function initBuffers(gl) {
  // Vertices
  var vertices = new Float32Array([
	/*   ______________
	 *  0              11
	 *	1____2    9____10
	 *       |    |
	 *   ____|    |____
	 *	4    3    8    7
	 *	5______________6
	*/

	-0.6,  1.0, 
	-0.6,  0.6,
	-0.2,  0.6,
	-0.2, -0.6,
	-0.6, -0.6,
	-0.6, -1.0,
	 0.6, -1.0,
	 0.6, -0.6,
	 0.2, -0.6,
	 0.2,  0.6,
	 0.6,  0.6,
	 0.6,  1.0
  ]);

  // Vertex colors
  var colors = new Float32Array([
	1.0, 0.0, 0.0,
	1.0, 0.0, 0.0,
	1.0, 0.0, 0.0,
	1.0, 0.0, 0.0,
	1.0, 0.0, 0.0,
	1.0, 0.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 1.0, 0.0,
	0.0, 1.0, 0.0
  ]);

  // Create, populate, and enable buffers
  initArrayBuffer(gl, vertices, 2, gl.FLOAT, 'a_VertexPosition');
  initArrayBuffer(gl, colors, 3, gl.FLOAT, 'a_Color');

  // Vertex Indices
  var vertexIndices = new Uint16Array([
	// strip indices [0, 8)
	0, 11, 2, 9, 3, 8, 6, 7,

	// triangle indices [8, 20)
	0, 1, 2,
	3, 4, 5,
	3, 5, 6,
	9, 10, 11,

	// outline indices [20, 43)
	// Line Loop
	0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,

	// Line Strip
	0, 2, 11, 9, 3, 6, 8, 3, 5,

	// Lines
	2, 9
  ]);

  // Push vertex indices to shader
  var verticesIndexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, verticesIndexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, vertexIndices, gl.STATIC_DRAW);
}

function initArrayBuffer(gl, data, num, type, attribute) {
  var buffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
  gl.bufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW);
  var a_attribute = gl.getAttribLocation(gl.program, attribute);
  gl.vertexAttribPointer(a_attribute, num, type, false, 0, 0);
  gl.enableVertexAttribArray(a_attribute);
}

var numFramesToAverage = 16;
var frameTimeHistory = [];
var frameTimeIndex = 0;
var totalTimeForFrames = 0;

var fpsElement = document.getElementById('fps');

var then = Date.now();
function animate(current_angle) {
  var ANGLE_STEP = 45.0;

  var now = Date.now() / 1000.0;
  var elapsed = now - then;
  then = now;

  totalTimeForFrames += elapsed - (frameTimeHistory[frameTimeIndex] || 0);
  frameTimeHistory[frameTimeIndex] = elapsed;
  frameTimeIndex = (frameTimeIndex + 1) % numFramesToAverage;

  var averageElapsedTime = totalTimeForFrames / numFramesToAverage;
  var fps = 1 / averageElapsedTime;
  fpsElement.innerHTML = fps.toFixed(0)

  return (current_angle + (elapsed * ANGLE_STEP)) % 360;
}

var mvMatrix = mat4.create();
var pMatrix = mat4.create();
var mvMatrixStack = [];

function mvPushMatrix() {
  var copy = mat4.create();
  mat4.copy(copy, mvMatrix);
  mvMatrixStack.push(copy);
}

function mvPopMatrix() {
  if (!mvMatrixStack.length) {
	throw("Can't pop from an empty matrix stack.");
  }

  mvMatrix = mvMatrixStack.pop();
}

function degToRad(degrees) {
  return degrees * Math.PI / 180.0;
}

function drawScene(gl, angle, u_MVMatrix, u_PMatrix) {
  // Clear the canvis
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  mat4.perspective(pMatrix, 45, 1, 0.1, 100.0);

  // Apply transformations
  mat4.identity(mvMatrix);

  mat4.translate(mvMatrix, mvMatrix, [0.0, 0.0, -4.0]);

  mvPushMatrix();

  var dX = Math.sin(degToRad(angle));
  var dY = Math.cos(degToRad(angle));

  mat4.translate(mvMatrix, mvMatrix, [dX, dY, dX]);

  mat4.rotateZ(mvMatrix, mvMatrix, degToRad(angle));
  mat4.rotateY(mvMatrix, mvMatrix, degToRad(angle));

  gl.uniformMatrix4fv(u_MVMatrix, false, mvMatrix);
  gl.uniformMatrix4fv(u_PMatrix, false, pMatrix);

  mvPopMatrix();

  // Draw block letter "I"
  var a_Color = gl.getAttribLocation(gl.program, 'a_Color');

  if (!outline) {
	// Enable vertex color buffer
	gl.enableVertexAttribArray(a_Color);

	gl.drawElements(gl.TRIANGLE_STRIP, 8, gl.UNSIGNED_SHORT, 0);
	gl.drawElements(gl.TRIANGLES, 12, gl.UNSIGNED_SHORT, 8*2);
  } else {
	// Disable vertex color buffer before drawing lines for the outline
	gl.disableVertexAttribArray(a_Color);

	// Define color and width of outline
	gl.vertexAttrib3f(a_Color, 1.0, 1.0, 1.0);
	gl.lineWidth(2.0);

	// Draw outline of block letter "I"
	gl.drawElements(gl.LINE_LOOP, 12, gl.UNSIGNED_SHORT, 20*2);

	// Draw outline of inner triangles
	gl.drawElements(gl.LINE_STRIP, 9, gl.UNSIGNED_SHORT, 32*2);
	gl.drawElements(gl.LINES, 2, gl.UNSIGNED_SHORT, 41*2);
  }
}
